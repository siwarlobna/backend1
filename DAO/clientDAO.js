const db = require('../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class clientDAO {
    constructor() {
    }

// Get gig list
    searchallusers(req, res, next) {
        console.log('searchAlllusers --')
        db['clients'].findAll()
            .then(client => res.json(client))
            .catch(err => console.log(err));
        return;
    };


// Search for gigs
    Searchusers(req, res, next) {
        var id = req.params.id;
        console.log(id)
        db['clients'].findByPk(id)
            .then(client => res.json(client))
            .catch(err => console.log(err));
        return;
    };


// Display add gig form

    addusers(req, res, next) {
        console.log('addddusers', req.body);

        var modalObj = db['clients'].build(req.body);
        // console.log(req.body);
        modalObj.save().then(result => {
            res.send('ok')
        })
    }

    updateusers (req, res,next) {
        // console.log('req', req.body)
        console.log('params', req.params)

        db['clients'].update(
                {email: req.body.email,
                tel:req.body.tel,
                adress:req.body.adress,
            },
            {where: {
                    id: req.params.id
                }}

        ).then(function(rowsUpdated) {
            res.json(rowsUpdated)
        })
            .catch(next)
        return;
    };


    deleteusers(req, res,next) {

        var id = req.params.id;
        db['clients'].destroy({where: {id: id}})
            .then(result => {
                res.send('ok')


            });

    }};
module.exports = clientDAO ;
