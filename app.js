var express = require('express'),
    bodyParser = require('body-parser'),

    path = require('path');
var cors = require('cors');






//var hookJWTStrategy = require('./services/passportStrategy');

var db = require('./models');

var app = express();
// the !! operator will evalute the ENV variable and if it's set it will get it's value and if not it will convert "undefined" to false
var sync = !!process.env.SYNC; // using "npm run prodsync" will run the app using the prod config and recreate the DB
// Parse as urlencoded and json.
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(cors());



//hookJWTStrategy(passport);

app.get('/', function (req, res) {
    res.send('hello world 3');
});
app.use('/api', require('./routes/api'));
app.listen(8500, function () {
    if (sync) {
        console.log("synchronizing database");
        db.sequelize.sync({force: true});
    }
});
