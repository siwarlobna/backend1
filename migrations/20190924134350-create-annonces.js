'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('annonces', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      photo: {
        type: Sequelize.STRING
      },
      detail: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      prix: {
        type: Sequelize.STRING
      },
      locale: {
        type: Sequelize.STRING
      },
      contact: {
        type: Sequelize.STRING
      },
      date: {
        type: Sequelize.STRING
      },
      id_client: {
        type: Sequelize.INTEGER
      },
      id_sousCategories: {
        type: Sequelize.INTEGER
      },
      id_user: {
        type: Sequelize.INTEGER
      },

    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('annonces');
  }
};
