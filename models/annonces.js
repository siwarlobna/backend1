'use strict';
module.exports = (sequelize, DataTypes) => {
  const annonces = sequelize.define('annonces', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    photo: DataTypes.STRING,
    detail: DataTypes.STRING,
    description: DataTypes.STRING,
    prix: DataTypes.STRING,
    locale: DataTypes.STRING,
    contact: DataTypes.STRING,
    date: DataTypes.STRING,
    id_client:DataTypes.INTEGER,
    id_sousCategories:DataTypes.INTEGER,
    id_user:DataTypes.INTEGER
  },  {
    timestamps: false
  }, {
    tableName: 'annonces'
  });
  annonces.associate = function(models) {
    // associations can be defined here
    annonces.belongsTo(models.users, {
      foreignKey: 'id_user'});
    annonces.belongsTo(models.sous_categories, {
      foreignKey: 'id_sousCategories'});

  };
  return annonces;
};
