'use strict';
module.exports = (sequelize, DataTypes) => {
  const categories = sequelize.define('categories', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nom: DataTypes.STRING,
    id_sous_categories:DataTypes.INTEGER
  },  {
    timestamps: false
  }, {
    tableName: 'categories'
  });
  categories.associate = function(models) {
    // associations can be defined here
    categories.hasMany(models.sous_categories, {
      foreignKey: 'id_sous_categories'
    });
  };
  return categories;
};
