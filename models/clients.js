'use strict';
module.exports = (sequelize, DataTypes) => {
  const clients = sequelize.define('clients', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    email: DataTypes.STRING,
    tel: DataTypes.STRING,
    adress: DataTypes.STRING,
    id_annonce:DataTypes.INTEGER,
  },  {
    timestamps: false
  }, {
    tableName: 'clients'
  });
  const users =require('./users')
clients.prototype.modelIncludes ={
    'users': {
      model:users
    }
  }

  clients.associate = function(models) {
   // associations can be defined here

  };
  return clients;
};
