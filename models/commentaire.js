'use strict';
module.exports = (sequelize, DataTypes) => {
  const commentaire = sequelize.define('commentaire', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    sujet: DataTypes.STRING,
    id_annonce:DataTypes.INTEGER,
    id_client:DataTypes.INTEGER

  },  {
    timestamps: false
  }, {
    tableName: 'commentaire'
  });
  commentaire.associate = function(models) {
    commentaire.belongsTo(models.annonces, {
      foreignKey: 'id_annonce'});
    commentaire.belongsTo(models.clients, {
      foreignKey: 'id_client'});
    // associations can be defined here
   };
  return commentaire;
};
