'use strict';
module.exports = (sequelize, DataTypes) => {
  const sous_categories = sequelize.define('sous_categories', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nom: DataTypes.STRING,
    id_annonces:DataTypes.INTEGER,
    id_categories:DataTypes.INTEGER
  },  {
    timestamps: false
  }, {
    tableName: 'sous_categories'
  });
  sous_categories.associate = function(models) {
    // associations can be defined here
    sous_categories.belongsTo(models.categories, {
      foreignKey: 'id_categories'
    });
    sous_categories.hasMany(models.annonces, {
      foreignKey: 'id_annonces'
    });
  };
  return sous_categories;
};
