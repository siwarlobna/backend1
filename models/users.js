'use strict';
module.exports = (sequelize, DataTypes) => {

  const users = sequelize.define('users', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nom: DataTypes.STRING,
    login: DataTypes.STRING,
    password: DataTypes.STRING,
    id_annonces:DataTypes.INTEGER
  },  {
    timestamps: false
  }, {
    tableName: 'users'
  });
  users.associate = function(models) {
    // associations can be defined here
    users.hasMany(models.annonces, {
      foreignKey: 'id_annonces'
    });
  };
  return users;
};
