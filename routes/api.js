'use strict';
const path = require('path');
const verify = require('../verifyToken')
var router = require('express').Router();
var usercontroller = require('../controller/usercontroller');
var clientcontroller = require('../controller/clientcontroller');
var categoriecontroller = require('../controller/categoriecontroller');
var souscategoriecontroller = require('../controller/souscategoriecontroller');
var annoncecontroller = require('../controller/annoncecontroller');
var commentairecontroller = require('../controller/commentairecontroller');
var articlecontroller =require('../controller/articleController')

    router.post('/add',usercontroller.addu);
    router.get('/search/:id',usercontroller.searchu);
    router.get('/searchall',verify, usercontroller.searchallu);
    router.put('/update/:id', usercontroller.updateu);
router.put('/updateuserps/:id', usercontroller.updateups);
router.post('/auth', usercontroller.auth);
router.post('/adda',articlecontroller.adda);
router.get('/searchalla',articlecontroller.searchalla);


    router.delete('/delete/:id', usercontroller.deleteu);

    router.post('/addc',clientcontroller.addu);
    router.get('/searchc/:id', clientcontroller.searchu);
    router.get('/searchallc', clientcontroller.searchallu);
    router.put('/updatec/:id', clientcontroller.updateu);
    router.delete('/deletec/:id', clientcontroller.deleteu);

    router.post('/addca',clientcontroller.addu);
    router.get('/searchca/:id',categoriecontroller.searchu);
    router.get('/searchallca', categoriecontroller.searchallu);
    router.put('/updateca/:id', categoriecontroller.updateu);
    router.delete('/deleteca/:id', categoriecontroller.deleteu);

    router.post('/adds',souscategoriecontroller.addu);
    router.get('/searchs/:id',souscategoriecontroller.searchu);
    router.get('/searchalls', souscategoriecontroller.searchallu);
    router.put('/updates/:id', souscategoriecontroller.updateu);
    router.delete('/deletes/:id', souscategoriecontroller.deleteu);

    router.post('/adda',annoncecontroller.addu);
    router.get('/searcha/:id²',annoncecontroller.searchu);
    router.get('/searchalla', annoncecontroller.searchallu);
    router.put('/updatea/:id', annoncecontroller.updateu);
    router.delete('/deletea/:id', annoncecontroller.deleteu);

    router.post('/addco',commentairecontroller.addu);
    router.get('/searchco/:id',commentairecontroller.searchu);
    router.get('/searchallco', commentairecontroller.searchallu);
    router.put('/updateco/:id', commentairecontroller.updateu);
    router.delete('/deleteco/:id', commentairecontroller.deleteu);




 module.exports = router;;
